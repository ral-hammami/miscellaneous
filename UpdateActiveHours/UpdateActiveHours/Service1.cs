﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace UpdateActiveHours
{
    public partial class UpdateActiveHours : ServiceBase
    {
        System.Timers.Timer timer;

        public UpdateActiveHours()
        {
            InitializeComponent();
        }

        int GetMillisecondsToNextRun()
        {
            DateTime dueTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Now.Hour + 1, 0, 0);
            TimeSpan timeRemaining = dueTime.Subtract(DateTime.Now);
            return timeRemaining.Milliseconds;
        }


        protected override void OnStart(string[] args)
        {
            timer = new System.Timers.Timer();
            timer.Interval = GetMillisecondsToNextRun();
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();
        }

        private void OnTimer(object sender, System.Timers.ElapsedEventArgs e)
        {
            timer.Interval = GetMillisecondsToNextRun();

            using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
            {
                using (var key = hklm.OpenSubKey(@"SOFTWARE\Microsoft\WindowsUpdate\UX\Settings", true))
                {
                    if (key != null)
                    {
                        key.SetValue("ActiveHoursStart", DateTime.Now.Hour - 1, RegistryValueKind.DWord);
                        key.SetValue("ActiveHoursEnd", DateTime.Now.Hour + 1, RegistryValueKind.DWord);
                    }
                }
            }
        }

        protected override void OnStop()
        {
        }
    }
}
