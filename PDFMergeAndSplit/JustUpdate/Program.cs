﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JustUpdate
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri uri = new Uri(args[0]);
            string appName = args[1];
            Version currentVersion = new Version(args[2]);
            string filename = "TemporaryExe";

            using (WebClient wc = new WebClient())
            {
                wc.DownloadFileAsync(uri, filename);
                FileVersionInfo versionInfo = FileVersionInfo.GetVersionInfo(filename);
                if (new Version(versionInfo.ProductVersion) > currentVersion)
                {
                    foreach (var process in Process.GetProcessesByName(appName))
                    {
                        process.Kill();
                    }

                    File.Move(String.Format("{0}.exe",appName),String.Format("{0}_Old.exe"));
                    File.Move(String.Format("{0}.exe", appName), String.Format("{0}.exe", appName));
                }
            }
        }
    }
}
