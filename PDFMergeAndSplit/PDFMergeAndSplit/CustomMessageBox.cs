﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDFMergeAndSplit
{
    public partial class CustomMessageBox : Form
    {
        public CustomMessageBox()
        {
            InitializeComponent();
        }

        public static DialogResult Show(string title, string text)
        {
            CustomMessageBox customMessageBox = new CustomMessageBox() { Text = title};
            customMessageBox.lblText.Text = text;
            return customMessageBox.ShowDialog();
        }
    }
}
