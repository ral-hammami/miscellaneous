﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PDFMergeAndSplit
{
    public partial class Form1 : Form
    {
        int index;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    Process.Start(AppDomain.CurrentDomain.BaseDirectory + @"JustUpdate.exe", "PDFMergeAndSplit");
            //}
            //catch
            //{
            //    // Do nothing here because the user should not be
            //    // interrupted if you misconfigure the updater.
            //}
        }

        private void btnMerge_Click(object sender, EventArgs e)
        {
            // Get some file names
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Multiselect = true;

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                lstFiles.Items.AddRange(ofd.FileNames);
            }
        }

        private void btnBeginMerge_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "PDF|.pdf";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string filename = sfd.FileName;

                // Open the output document
                PdfDocument outputDocument = new PdfDocument();

                // Iterate files
                foreach (string file in lstFiles.Items)
                {
                    PdfDocument inputDocument;

                    try
                    {
                        // Open the document to import pages from it.
                        inputDocument = PdfReader.Open(file, PdfDocumentOpenMode.Import);
                    }
                    catch (Exception ex)
                    {
                        ReplaceFileStream(file);

                        inputDocument = PdfReader.Open(file, PdfDocumentOpenMode.Import);
                    }

                    // Iterate pages
                    int count = inputDocument.PageCount;
                    for (int idx = 0; idx < count; idx++)
                    {
                        // Get the page from the external document...
                        PdfPage page = inputDocument.Pages[idx];
                        // ...and add it to the output document.
                        outputDocument.AddPage(page);
                    }
                }

                // Save the document...
                outputDocument.Save(filename);
                // ...and start a viewer.
                Process.Start(filename);
            }
        }

        private void ReplaceFileStream(string file)
        {
            byte[] fileBytes = File.ReadAllBytes(file);

            //25 50 44 46 2D 31 2E 34
            fileBytes[0] = 0x25;
            fileBytes[1] = 0x50;
            fileBytes[2] = 0x44;            
            fileBytes[3] = 0x46;
            fileBytes[4] = 0x2D;
            fileBytes[5] = 0x31;
            fileBytes[6] = 0x2E;
            fileBytes[7] = 0x34;

            File.WriteAllBytes(file, fileBytes);
        }

        //Using memoryStream As MemoryStream = ReturnCompatiblePdf(File.FullName)
        //Dim DocPdf As PdfDocument = PdfReader.Open(memoryStream, PdfDocumentOpenMode.Import)
        ////Your code here.....
        //End Using

        private void lstFiles_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                index = lstFiles.IndexFromPoint(e.Location);
            }
            else
            {
                if (this.lstFiles.SelectedItem == null) return;
                this.lstFiles.DoDragDrop(this.lstFiles.SelectedItem, DragDropEffects.Move);
            }
        }

        private void lstFiles_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void lstFiles_DragDrop(object sender, DragEventArgs e)
        {
            Point point = lstFiles.PointToClient(new Point(e.X, e.Y));
            int index = this.lstFiles.IndexFromPoint(point);
            if (index < 0) index = this.lstFiles.Items.Count - 1;
            object data = lstFiles.SelectedItem;
            this.lstFiles.Items.Remove(data);
            this.lstFiles.Items.Insert(index, data);
        }

        private void btnSplit_Click(object sender, EventArgs e)
        {
            // Get a fresh copy of the sample PDF file
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string filename = ofd.FileName;

                // Open the file
                PdfDocument inputDocument = PdfReader.Open(filename, PdfDocumentOpenMode.Import);

                string name = Path.GetFileNameWithoutExtension(filename);
                for (int idx = 0; idx < inputDocument.PageCount; idx++)
                {
                    // Create new document
                    PdfDocument outputDocument = new PdfDocument();
                    outputDocument.Version = inputDocument.Version;
                    outputDocument.Info.Title =
                      String.Format("Page {0} of {1}", idx + 1, inputDocument.Info.Title);
                    outputDocument.Info.Creator = inputDocument.Info.Creator;

                    // Add the page and save it
                    outputDocument.AddPage(inputDocument.Pages[idx]);
                    outputDocument.Save(String.Format("{0}\\{1} - Page {2}.pdf", Path.GetDirectoryName(ofd.FileName), name, idx + 1));
                }

                CustomMessageBox.Show("PDF Split", "Split complete!");
                string argument = "/select, \"" + ofd.FileName + "\"";
                System.Diagnostics.Process.Start("explorer.exe", argument);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            lstFiles.Items.RemoveAt(index);
        }

        private void tabPage2_Enter(object sender, EventArgs e)
        {
            this.Size = new Size(172, 107);
        }

        private void tabPage1_Enter(object sender, EventArgs e)
        {
            this.Size = new Size(214, 325);
        }
    }
}
