﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        static String SheetId = "1zR-GW4GjgpmKSdmbDS7MlwM2gOIbGfqCzKpLIYxOZJc";

        // If modifying these scopes, delete your previously saved credentials
        // at ~/.credentials/sheets.googleapis.com-dotnet-quickstart.json
        static string[] Scopes = { SheetsService.Scope.Spreadsheets };
        static string ApplicationName = "Google Sheets API .NET Quickstart";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Nothing for now.
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            UserCredential sheetsCredential;

            using (var stream =
                new FileStream("client_id.json", FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = "token.json";
                sheetsCredential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                 GoogleClientSecrets.Load(stream).Secrets,
                 Scopes,
                 "user",
                 CancellationToken.None,
                 new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // Create Google Sheets API service.
            var sheetsService = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = sheetsCredential,
                ApplicationName = ApplicationName,
            });

            // Define request parameters.
            //String uniformRegisterId = "261626562";
            //String uniformSalesId = "1119658731";
            //String registerHistoryId = "1456058284";

            //sheet1.getRange("T1:AA5").copyTo((sheet2.getRange(maxIndex + 2, 2)), {contentsOnly:true});
            //sheet1.getRange("T1:AK5").copyTo((sheet3.getRange(maxIndex2 + 2, 1)), {contentsOnly:true});

            IList<IList<Object>> values = GetValues(sheetsService);
            string newRange = GetUniformSalesRange(sheetsService);
            UpdateGoogleSheetinBatch(values, SheetId, newRange, sheetsService);
        }

        private void UpdateGoogleSheetinBatch(IList<IList<Object>> values, string spreadsheetId, string newRange, SheetsService service)
        {
            SpreadsheetsResource.ValuesResource.AppendRequest request =
               service.Spreadsheets.Values.Append(new ValueRange() { Values = values }, spreadsheetId, newRange);
            request.InsertDataOption = SpreadsheetsResource.ValuesResource.AppendRequest.InsertDataOptionEnum.INSERTROWS;
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            var response = request.Execute();
        }

        //TODO 1: Need to figure out how to get the data in the same format as I would be had the sheet been submitted. For example, price will have to be derived from other named ranges.
        private IList<IList<Object>> GetValues(SheetsService service)
        {
            IList<IList<Object>> values = new List<IList<Object>>();
            if (!String.IsNullOrEmpty(ddlShirt.Text))
            {
                values.Add(new List<Object>{dateTimePickerPurchase.Value.ToString("MM/dd/yyyy"),txtFirst.Text,txtLast.Text,ddlTransactionType.Text,"SHIRT",ddlShirt.Text,numericUpDownShirt.Value,ddlPayrollDeduction.Text});
            }
            if (!String.IsNullOrEmpty(ddlJacket.Text))
            {
                values.Add(new List<Object> { dateTimePickerPurchase.Value.ToString("MM/dd/yyyy"), txtFirst.Text, txtLast.Text, ddlTransactionType.Text, "SHIRT", ddlJacket.Text, numericUpDownJacket.Value, ddlPayrollDeduction.Text });
            }
            if (!String.IsNullOrEmpty(ddlShorts.Text))
            {
                values.Add(new List<Object> { dateTimePickerPurchase.Value.ToString("MM/dd/yyyy"), txtFirst.Text, txtLast.Text, ddlTransactionType.Text, "SHIRT", ddlShorts.Text, numericUpDownShorts.Value, ddlPayrollDeduction.Text });
            }
            if (!String.IsNullOrEmpty(ddlPants.Text))
            {
                values.Add(new List<Object> { dateTimePickerPurchase.Value.ToString("MM/dd/yyyy"), txtFirst.Text, txtLast.Text, ddlTransactionType.Text, "SHIRT", ddlPants.Text, numericUpDownPants.Value, ddlPayrollDeduction.Text });
            }
            if (!String.IsNullOrEmpty(ddlHat.Text))
            {
                values.Add(new List<Object> { dateTimePickerPurchase.Value.ToString("MM/dd/yyyy"), txtFirst.Text, txtLast.Text, ddlTransactionType.Text, "SHIRT", ddlHat.Text, numericUpDownHat.Value, ddlPayrollDeduction.Text });
            }

            return values;
        }

        protected static string GetUniformSalesRange(SheetsService service)
        {
            // Define request parameters.
            String spreadsheetId = SheetId;
            String range = "UNIFORM SALES!B:I";
            SpreadsheetsResource.ValuesResource.GetRequest getRequest =
                       service.Spreadsheets.Values.Get(spreadsheetId, range);
            ValueRange getResponse = getRequest.Execute();
            IList<IList<Object>> getValues = getResponse.Values;
            int currentCount = getValues.Count() + 2;
            String newRange = String.Format("UNIFORM SALES!B{0}:I",currentCount);
            return newRange;
        }

        protected static string GetRegisterHistoryRange(SheetsService service)
        {
            // Define request parameters.
            String spreadsheetId = SheetId;
            String range = "UNIFORM SALES!B:I";
            SpreadsheetsResource.ValuesResource.GetRequest getRequest =
                       service.Spreadsheets.Values.Get(spreadsheetId, range);
            ValueRange getResponse = getRequest.Execute();
            IList<IList<Object>> getValues = getResponse.Values;
            int currentCount = getValues.Count() + 2;
            String newRange = String.Format("UNIFORM SALES!B{0}:I", currentCount);
            return newRange;
        }
    }
}
