﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtFirst = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLast = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnSubmit = new System.Windows.Forms.Button();
            this.ddlDepartment = new System.Windows.Forms.ComboBox();
            this.ddlStatus = new System.Windows.Forms.ComboBox();
            this.ddlShirt = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numericUpDownShirt = new System.Windows.Forms.NumericUpDown();
            this.ddlAllShirtsReceived = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.ddlSizing = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.ddlAllJacketsReceived = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDownJacket = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.ddlJacket = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.ddlAllPantsReceived = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.numericUpDownPants = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.ddlPants = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ddlAllShortsReceived = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDownShorts = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.ddlShorts = new System.Windows.Forms.ComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.ddlAllHatsReceived = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.numericUpDownHat = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.ddlHat = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.ddlPayrollDeduction = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.dateTimePickerPurchase = new System.Windows.Forms.DateTimePicker();
            this.label22 = new System.Windows.Forms.Label();
            this.ddlTransactionType = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShirt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJacket)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShorts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHat)).BeginInit();
            this.SuspendLayout();
            // 
            // txtFirst
            // 
            this.txtFirst.Location = new System.Drawing.Point(129, 18);
            this.txtFirst.Name = "txtFirst";
            this.txtFirst.Size = new System.Drawing.Size(100, 20);
            this.txtFirst.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(23, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "First";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(23, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "Last";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtLast
            // 
            this.txtLast.Location = new System.Drawing.Point(129, 44);
            this.txtLast.Name = "txtLast";
            this.txtLast.Size = new System.Drawing.Size(100, 20);
            this.txtLast.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(23, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "Department";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(23, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Status";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnSubmit
            // 
            this.btnSubmit.Location = new System.Drawing.Point(383, 291);
            this.btnSubmit.Name = "btnSubmit";
            this.btnSubmit.Size = new System.Drawing.Size(75, 23);
            this.btnSubmit.TabIndex = 8;
            this.btnSubmit.Text = "Submit";
            this.btnSubmit.UseVisualStyleBackColor = true;
            this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
            // 
            // ddlDepartment
            // 
            this.ddlDepartment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlDepartment.FormattingEnabled = true;
            this.ddlDepartment.Items.AddRange(new object[] {
            "AIRPORT VALET",
            "EVENT VALET",
            "MAINTENANCE",
            "OFFICE",
            "TEMP-VALET",
            "TEMP-MAINT"});
            this.ddlDepartment.Location = new System.Drawing.Point(129, 73);
            this.ddlDepartment.Name = "ddlDepartment";
            this.ddlDepartment.Size = new System.Drawing.Size(121, 21);
            this.ddlDepartment.TabIndex = 2;
            // 
            // ddlStatus
            // 
            this.ddlStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlStatus.FormattingEnabled = true;
            this.ddlStatus.Items.AddRange(new object[] {
            "CURRENT EMPLOYEE",
            "NEW HIRE"});
            this.ddlStatus.Location = new System.Drawing.Point(129, 100);
            this.ddlStatus.Name = "ddlStatus";
            this.ddlStatus.Size = new System.Drawing.Size(121, 21);
            this.ddlStatus.TabIndex = 3;
            // 
            // ddlShirt
            // 
            this.ddlShirt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlShirt.FormattingEnabled = true;
            this.ddlShirt.Items.AddRange(new object[] {
            "AWESOME"});
            this.ddlShirt.Location = new System.Drawing.Point(430, 21);
            this.ddlShirt.Name = "ddlShirt";
            this.ddlShirt.Size = new System.Drawing.Size(121, 21);
            this.ddlShirt.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(324, 21);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "Shirt";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(324, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Shirt Quantity";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // numericUpDownShirt
            // 
            this.numericUpDownShirt.Location = new System.Drawing.Point(430, 48);
            this.numericUpDownShirt.Name = "numericUpDownShirt";
            this.numericUpDownShirt.Size = new System.Drawing.Size(37, 20);
            this.numericUpDownShirt.TabIndex = 6;
            // 
            // ddlAllShirtsReceived
            // 
            this.ddlAllShirtsReceived.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAllShirtsReceived.FormattingEnabled = true;
            this.ddlAllShirtsReceived.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.ddlAllShirtsReceived.Location = new System.Drawing.Point(430, 74);
            this.ddlAllShirtsReceived.Name = "ddlAllShirtsReceived";
            this.ddlAllShirtsReceived.Size = new System.Drawing.Size(121, 21);
            this.ddlAllShirtsReceived.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(324, 74);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 17);
            this.label8.TabIndex = 19;
            this.label8.Text = "All Shirts Reveived";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlSizing
            // 
            this.ddlSizing.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlSizing.FormattingEnabled = true;
            this.ddlSizing.Items.AddRange(new object[] {
            "MALE",
            "FEMALE"});
            this.ddlSizing.Location = new System.Drawing.Point(129, 127);
            this.ddlSizing.Name = "ddlSizing";
            this.ddlSizing.Size = new System.Drawing.Size(121, 21);
            this.ddlSizing.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(23, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 17);
            this.label5.TabIndex = 21;
            this.label5.Text = "Sizing";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlAllJacketsReceived
            // 
            this.ddlAllJacketsReceived.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAllJacketsReceived.FormattingEnabled = true;
            this.ddlAllJacketsReceived.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.ddlAllJacketsReceived.Location = new System.Drawing.Point(680, 74);
            this.ddlAllJacketsReceived.Name = "ddlAllJacketsReceived";
            this.ddlAllJacketsReceived.Size = new System.Drawing.Size(121, 21);
            this.ddlAllJacketsReceived.TabIndex = 24;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(561, 74);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(113, 17);
            this.label9.TabIndex = 27;
            this.label9.Text = "All Jackets Reveived";
            this.label9.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // numericUpDownJacket
            // 
            this.numericUpDownJacket.Location = new System.Drawing.Point(680, 48);
            this.numericUpDownJacket.Name = "numericUpDownJacket";
            this.numericUpDownJacket.Size = new System.Drawing.Size(37, 20);
            this.numericUpDownJacket.TabIndex = 23;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(574, 50);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 17);
            this.label10.TabIndex = 26;
            this.label10.Text = "Jacket Quantity";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlJacket
            // 
            this.ddlJacket.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlJacket.FormattingEnabled = true;
            this.ddlJacket.Items.AddRange(new object[] {
            "AWESOME"});
            this.ddlJacket.Location = new System.Drawing.Point(680, 21);
            this.ddlJacket.Name = "ddlJacket";
            this.ddlJacket.Size = new System.Drawing.Size(121, 21);
            this.ddlJacket.TabIndex = 22;
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(574, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(100, 17);
            this.label11.TabIndex = 25;
            this.label11.Text = "Jacket";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlAllPantsReceived
            // 
            this.ddlAllPantsReceived.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAllPantsReceived.FormattingEnabled = true;
            this.ddlAllPantsReceived.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.ddlAllPantsReceived.Location = new System.Drawing.Point(680, 165);
            this.ddlAllPantsReceived.Name = "ddlAllPantsReceived";
            this.ddlAllPantsReceived.Size = new System.Drawing.Size(121, 21);
            this.ddlAllPantsReceived.TabIndex = 36;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(561, 165);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(113, 17);
            this.label12.TabIndex = 39;
            this.label12.Text = "All Pants Reveived";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // numericUpDownPants
            // 
            this.numericUpDownPants.Location = new System.Drawing.Point(680, 139);
            this.numericUpDownPants.Name = "numericUpDownPants";
            this.numericUpDownPants.Size = new System.Drawing.Size(37, 20);
            this.numericUpDownPants.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(574, 141);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(100, 17);
            this.label13.TabIndex = 38;
            this.label13.Text = "Pants Quantity";
            this.label13.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlPants
            // 
            this.ddlPants.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlPants.FormattingEnabled = true;
            this.ddlPants.Items.AddRange(new object[] {
            "AWESOME"});
            this.ddlPants.Location = new System.Drawing.Point(680, 112);
            this.ddlPants.Name = "ddlPants";
            this.ddlPants.Size = new System.Drawing.Size(121, 21);
            this.ddlPants.TabIndex = 34;
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(574, 112);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 17);
            this.label14.TabIndex = 37;
            this.label14.Text = "Pants";
            this.label14.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlAllShortsReceived
            // 
            this.ddlAllShortsReceived.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAllShortsReceived.FormattingEnabled = true;
            this.ddlAllShortsReceived.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.ddlAllShortsReceived.Location = new System.Drawing.Point(430, 165);
            this.ddlAllShortsReceived.Name = "ddlAllShortsReceived";
            this.ddlAllShortsReceived.Size = new System.Drawing.Size(121, 21);
            this.ddlAllShortsReceived.TabIndex = 30;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(324, 165);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(100, 17);
            this.label15.TabIndex = 33;
            this.label15.Text = "All Shorts Reveived";
            this.label15.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // numericUpDownShorts
            // 
            this.numericUpDownShorts.Location = new System.Drawing.Point(430, 139);
            this.numericUpDownShorts.Name = "numericUpDownShorts";
            this.numericUpDownShorts.Size = new System.Drawing.Size(37, 20);
            this.numericUpDownShorts.TabIndex = 29;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(324, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(100, 17);
            this.label16.TabIndex = 32;
            this.label16.Text = "Shorts Quantity";
            this.label16.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlShorts
            // 
            this.ddlShorts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlShorts.FormattingEnabled = true;
            this.ddlShorts.Items.AddRange(new object[] {
            "AWESOME"});
            this.ddlShorts.Location = new System.Drawing.Point(430, 112);
            this.ddlShorts.Name = "ddlShorts";
            this.ddlShorts.Size = new System.Drawing.Size(121, 21);
            this.ddlShorts.TabIndex = 28;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(324, 112);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(100, 17);
            this.label17.TabIndex = 31;
            this.label17.Text = "Shorts";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlAllHatsReceived
            // 
            this.ddlAllHatsReceived.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlAllHatsReceived.FormattingEnabled = true;
            this.ddlAllHatsReceived.Items.AddRange(new object[] {
            "YES",
            "NO"});
            this.ddlAllHatsReceived.Location = new System.Drawing.Point(430, 253);
            this.ddlAllHatsReceived.Name = "ddlAllHatsReceived";
            this.ddlAllHatsReceived.Size = new System.Drawing.Size(121, 21);
            this.ddlAllHatsReceived.TabIndex = 42;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(311, 253);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(113, 17);
            this.label18.TabIndex = 45;
            this.label18.Text = "All Hats Reveived";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // numericUpDownHat
            // 
            this.numericUpDownHat.Location = new System.Drawing.Point(430, 227);
            this.numericUpDownHat.Name = "numericUpDownHat";
            this.numericUpDownHat.Size = new System.Drawing.Size(37, 20);
            this.numericUpDownHat.TabIndex = 41;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(324, 229);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(100, 17);
            this.label19.TabIndex = 44;
            this.label19.Text = "Hat Quantity";
            this.label19.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlHat
            // 
            this.ddlHat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlHat.FormattingEnabled = true;
            this.ddlHat.Items.AddRange(new object[] {
            "AWESOME"});
            this.ddlHat.Location = new System.Drawing.Point(430, 200);
            this.ddlHat.Name = "ddlHat";
            this.ddlHat.Size = new System.Drawing.Size(121, 21);
            this.ddlHat.TabIndex = 40;
            // 
            // label20
            // 
            this.label20.Location = new System.Drawing.Point(324, 200);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(100, 17);
            this.label20.TabIndex = 43;
            this.label20.Text = "Hat";
            this.label20.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlPayrollDeduction
            // 
            this.ddlPayrollDeduction.FormattingEnabled = true;
            this.ddlPayrollDeduction.Location = new System.Drawing.Point(129, 154);
            this.ddlPayrollDeduction.Name = "ddlPayrollDeduction";
            this.ddlPayrollDeduction.Size = new System.Drawing.Size(121, 21);
            this.ddlPayrollDeduction.TabIndex = 46;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(23, 157);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(100, 17);
            this.label21.TabIndex = 47;
            this.label21.Text = "Payroll Deduction";
            this.label21.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // dateTimePickerPurchase
            // 
            this.dateTimePickerPurchase.Location = new System.Drawing.Point(129, 181);
            this.dateTimePickerPurchase.Name = "dateTimePickerPurchase";
            this.dateTimePickerPurchase.Size = new System.Drawing.Size(180, 20);
            this.dateTimePickerPurchase.TabIndex = 48;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(23, 184);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(100, 17);
            this.label22.TabIndex = 49;
            this.label22.Text = "Purchase Date";
            this.label22.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ddlTransactionType
            // 
            this.ddlTransactionType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddlTransactionType.FormattingEnabled = true;
            this.ddlTransactionType.Items.AddRange(new object[] {
            "SALE"});
            this.ddlTransactionType.Location = new System.Drawing.Point(129, 204);
            this.ddlTransactionType.Name = "ddlTransactionType";
            this.ddlTransactionType.Size = new System.Drawing.Size(121, 21);
            this.ddlTransactionType.TabIndex = 50;
            // 
            // label23
            // 
            this.label23.Location = new System.Drawing.Point(23, 204);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(100, 17);
            this.label23.TabIndex = 51;
            this.label23.Text = "Transaction Type";
            this.label23.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(841, 326);
            this.Controls.Add(this.ddlTransactionType);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.dateTimePickerPurchase);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.ddlPayrollDeduction);
            this.Controls.Add(this.ddlAllHatsReceived);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.numericUpDownHat);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.ddlHat);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.ddlAllPantsReceived);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.numericUpDownPants);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.ddlPants);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.ddlAllShortsReceived);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.numericUpDownShorts);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.ddlShorts);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.ddlAllJacketsReceived);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.numericUpDownJacket);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.ddlJacket);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.ddlSizing);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.ddlAllShirtsReceived);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numericUpDownShirt);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ddlShirt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.ddlStatus);
            this.Controls.Add(this.ddlDepartment);
            this.Controls.Add(this.btnSubmit);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtLast);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFirst);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShirt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownJacket)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownPants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownShorts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownHat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtFirst;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtLast;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnSubmit;
        private System.Windows.Forms.ComboBox ddlDepartment;
        private System.Windows.Forms.ComboBox ddlStatus;
        private System.Windows.Forms.ComboBox ddlShirt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDownShirt;
        private System.Windows.Forms.ComboBox ddlAllShirtsReceived;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox ddlSizing;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ddlAllJacketsReceived;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numericUpDownJacket;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox ddlJacket;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox ddlAllPantsReceived;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown numericUpDownPants;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox ddlPants;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox ddlAllShortsReceived;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numericUpDownShorts;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.ComboBox ddlShorts;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox ddlAllHatsReceived;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.NumericUpDown numericUpDownHat;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox ddlHat;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox ddlPayrollDeduction;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DateTimePicker dateTimePickerPurchase;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox ddlTransactionType;
        private System.Windows.Forms.Label label23;
    }
}

